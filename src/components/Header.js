import React from "react";
import './Style.css'
import arrow from "../assets/right-arrow.svg"
import {Link} from "react-router-dom";
import {FaTelegramPlane} from "react-icons/fa";
import {AiFillInstagram} from "react-icons/ai";
import {CgFacebook} from "react-icons/cg";



export default function Header(props) {

    return (
        <header id="header">
            <div className="intro">
                <div className="overlay">
                    <div className="container">
                        <div className="header-padding row">
                            <h1 className="display-1 mb-5">
                                {props.data ? props.data.title : "Loading"}
                            </h1>
                            <div className="pt-5 w-100">
                                <p className="paragraph-header mt-5 mb-0">
                                    {props.data? props.data.paragraph: "Loading"}
                                </p>
                                <div className="d-flex">
                                    <a
                                        href="#features"
                                        className="btn btn-more btn-lg mt-4"
                                    >
                                        More
                                        <img src={arrow} alt=""/>
                                    </a>
                                    <ul className="list-unstyled d-flex my-5 ml-auto">
                                        <li className="mr-2">
                                            <Link to="https://telegram.org/">
                                                <FaTelegramPlane size={25} color={'#ffffff'}/>
                                            </Link>
                                        </li>
                                        <li className="mr-2">
                                            <Link to="https://instagram.com/">
                                                <AiFillInstagram size={25} color={'#ffffff'}/>
                                            </Link>
                                        </li>
                                        <li className="mr-2">
                                            <Link to="https://facebook.com/">
                                                <CgFacebook size={25} color={'#ffffff'}/>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}


