import React, {useState, useEffect} from 'react'
import Navigation from './components/Navigation';
import Header from './components/Header';
import About from './components/About';
import Services from './components/Services';
import Portfolio from './components/Portfolio';
import Testimonials from './components/Testimonials';
import Team from './components/Team';
import Contact from './components/Contact';
import JsonData from './data/data.json';

export default function App() {
    const [landingPageData, setLandingPageData] = useState({})


    function getLandingPageData() {
        setLandingPageData(JsonData)
    }

    useEffect(() => {
        getLandingPageData();
    }, [])


    return (
        <div>
            <Navigation/>
            <Header data={landingPageData.Header}/>
            <About data={landingPageData.About}/>
            <Portfolio data={landingPageData.Portfolio}/>
            <Services data={landingPageData.Services}/>
            <Testimonials data={landingPageData.Testimonials}/>
            <Contact data={landingPageData.Contact}/>
            <Team data={landingPageData.Team}/>
        </div>
    )

}

